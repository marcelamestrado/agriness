from flask import Flask, jsonify
from flask_restful import Api
from pymongo import MongoClient
import json
from bson import json_util

from libraryapi.resources.client import Client
from libraryapi.resources.book import Book
from libraryapi.resources.emprestimo import Emprestimo

from libraryapi.models.ClientModel import ClientModel
from libraryapi.models.BookModel import BookModel
from libraryapi.models.EmprestimoModel import EmprestimoModel

app = Flask(__name__)
api = Api(app)

dbclient = MongoClient('mongodb://localhost:27017/')
db = dbclient["books"]

# api.add_resource(Client, "/client", "/client/<string:id>")
api.add_resource(Client, "/client", "/client/<string:id>/books")
api.add_resource(Book, "/book", "/book")
api.add_resource(Emprestimo, "/emprestimo", "/emprestimo")


def insertClients():

    data = []
    with open("../clients.json") as f:
        try:
            data = json.load(f)
        except:
            users = {}
    if db["clients"].find().count() == 0:
        for item in [ClientModel(x).data() for x in data]:
            db["clients"].insert(item)


def insertBooks():

    data = []
    with open("../books.json") as f:
        try:
            data = json.load(f)
        except:
            users = {}
    if db["books"].find().count() == 0:
        for item in [BookModel(x).data() for x in data]:
            db["books"].insert(item)


def insertEmprestimos():

    data = []
    with open("../emprestimos.json") as f:
        try:
            data = json.load(f)
        except:
            users = {}
    if db["emprestimos"].find().count() == 0:
        for item in [EmprestimoModel(x).data() for x in data]:
            db["emprestimos"].insert(item)


if __name__ == '__main__':
    insertClients()
    insertBooks()
    insertEmprestimos()

    app.run(debug=True)
