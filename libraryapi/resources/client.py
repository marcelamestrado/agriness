from pymongo import MongoClient
from flask import Flask, request, jsonify
from flask_restful import fields, marshal_with, reqparse, Resource
import json
from bson import json_util, ObjectId
import time
import datetime

from libraryapi.models.ClientModel import ClientModel
from libraryapi.models.EmprestimoModel import EmprestimoModel


dbclient = MongoClient('mongodb://localhost:27017/')
db = dbclient["books"]


class Client(Resource):
    def post(self):
        clients = db["clients"]

        response = clients.insert_one(request.json)
        result = clients.find_one(response.inserted_id)
        return jsonify(
            {
                "sucess": True,
                "message": "Cliente inserido com sucesso!",
                "data": ClientModel(result).getdict()
            }
        )

    def get(self):
        clients = db["clients"]
        return jsonify(json.loads(json_util.dumps(clients.find())))

    def get(self, id):
        emprestimos = db["emprestimos"]
        emprestimo = emprestimos.find_one({"client": ObjectId(id)})
        model = EmprestimoModel(emprestimo)
        return jsonify(model.getdict())

    @classmethod
    def convetTime(self, epoch):
        return time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime(epoch))

