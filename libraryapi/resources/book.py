from pymongo import MongoClient
from flask import Flask, request, jsonify
from flask_restful import fields, marshal_with, reqparse, Resource
# from bson.json_util import dumps, loads
import json
from bson import json_util, ObjectId
import time

from libraryapi.models.BookModel import BookModel

dbclient = MongoClient('mongodb://localhost:27017/')
db = dbclient["books"]


class Book(Resource):

    def get(self):
        books = db["books"]

        return jsonify([BookModel(x).getdict() for x in books.find()])


    def post(self):
        books = db["books"]
        data = {
            "isbn": request.json["isbn"],
            "title": request.json["title"],
            "subtitle": request.json["subtitle"],
            "author": request.json["author"],
            "published": request.json["published"],
            "publisher": request.json["publisher"],
            "pages": request.json["pages"],
            "description": request.json["description"],
            "website": request.json["website"],
            "status": False
        }
        response = books.insert_one(data)
        result = books.find_one(response.inserted_id)
        return jsonify(
            {
                "sucess": True,
                "message": "Livro inserido com sucesso!",
                "data": {
                    "_id": str(result["_id"]),
                    "isbn": result["isbn"],
                    "title": result["title"],
                    "subtitle": result["subtitle"],
                    "author": result["author"],
                    "published": result["published"],
                    "publisher": result["publisher"],
                    "pages": result["pages"],
                    "description": result["description"],
                    "website": result["website"],
                    "status": result["status"]
                }
            }
        )
