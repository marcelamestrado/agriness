from pymongo import MongoClient
from flask import Flask, request, jsonify
from flask_restful import fields, marshal_with, reqparse, Resource
from bson.dbref import DBRef
# from bson.json_util import dumps, loads
import json
from bson import json_util, ObjectId
import time
import datetime
from libraryapi.models.EmprestimoModel import EmprestimoModel


dbclient = MongoClient('mongodb://localhost:27017/')
db = dbclient["books"]


class Emprestimo(Resource):
    def post(self):
        emprestimo = db["emprestimos"]

        model = EmprestimoModel(request.json)
        response = emprestimo.insert_one(model.getData())
        result = emprestimo.find_one(response.inserted_id)

        return jsonify(
            {
                "sucess": True,
                "message": "Emprestimo efetuado com sucesso!",
                "data": EmprestimoModel(result).getdict()
            }
        )

    def get(self, id):
        emprestimo = db["emprestimos"]
        emprestimo = emprestimo.find_one({"client": ObjectId(id)})
