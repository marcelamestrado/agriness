from bson import json_util, ObjectId


class ClientModel:
    def __init__(self, Client=None):
        self._id = self.setId(Client)
        self._nome = Client["nome"] if "nome" in Client else None
        self._email = Client["email"] if "email" in Client else None
        self._cidade = Client["cidade"] if "cidade" in Client else None
        self._celular = Client["celular"] if "celular" in Client else None

    def setId(self, client):
        if "_id" in client:
            try:
                key = client["_id"]["$oid"]
                return key
            except:
                return str(client["_id"])
        return None


    def getdict(self):
        return {
            "_id": self._id,
            "nome": self._nome,
            "email": self._email,
            "cidade": self._cidade,
            "celular": self._celular,
        }

    def data(self):
        return {
            "_id": ObjectId(self._id),
            "nome": self._nome,
            "email": self._email,
            "cidade": self._cidade,
            "celular": self._celular,
        }
