import time
from bson import json_util, ObjectId


class BookModel:
    def __init__(self, Book=None):
        self._id = self.setId(Book)
        self._isbn = Book["isbn"] if "isbn" in Book else None
        self._title = Book["title"] if "title" in Book else None
        self._subtitle = Book["subtitle"] if "subtitle" in Book else None
        self._author = Book["author"] if "author" in Book else None
        self._published = Book["published"] if "publishe" in Book else None
        self._publisher = Book["publisher"] if "publisher" in Book else None
        self._pages = Book["pages"] if "pages" in Book else None
        self._description = Book["description"] if "description" in Book else None
        self._website = Book["website"] if "website" in Book else None
        self._status = Book["status"] if "status" in Book else None

    def setId(self, book):
        if "_id" in book:
            try:
                key = book["_id"]["$oid"]
                return key
            except:
                return str(book["_id"])
        return None

    def getdict(self):
        return {
            "_id": self._id,
            "isbn": self._isbn,
            "title": self._title,
            "subtitle": self._subtitle,
            "author": self._author,
            "published": self._published,
            "publisher": self._publisher,
            "pages": self._pages,
            "description": self._description,
            "website": self._website,
            "status": self._status,
        }

    def data(self):
        return {
            "_id": ObjectId(self._id),
            "isbn": self._isbn,
            "title": self._title,
            "subtitle": self._subtitle,
            "author": self._author,
            "published": self._published,
            "publisher": self._publisher,
            "pages": self._pages,
            "description": self._description,
            "website": self._website,
            "status": self._status,
        }

    def showhTime(self):
        return time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime(self._emprestimo))
