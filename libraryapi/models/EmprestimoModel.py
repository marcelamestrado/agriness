import time
from pymongo import MongoClient
from bson import json_util, ObjectId
from libraryapi.models.BookModel import BookModel

dbclient = MongoClient('mongodb://localhost:27017/')
db = dbclient["books"]


class EmprestimoModel:
    def __init__(self, emprestimo=None):
        self._id = self.setId(emprestimo)
        self._client = self.setIdClients(emprestimo) if "client" in emprestimo else None
        self._books = [ObjectId(x) for x in emprestimo["books"]] if "books" in emprestimo else None
        self._data = emprestimo["data"] if "data" in emprestimo else time.time()
        self._valor = emprestimo["valor"] if "valor" in emprestimo else float(0.0)

    def setId(self, emprestimo):
        if "_id" in emprestimo:
            try:
                key = emprestimo["_id"]["$oid"]
                return key
            except:
                return str(emprestimo["_id"])
        return None

    def setIdClients(self, client):
        if "_id" in client:
            try:
                key = client["_id"]["$oid"]
                return key
            except:
                return str(client["_id"])
        if "$oid" in client:
            return ObjectId(client["$oid"])
        return None

    def setIdBook(self, book):
        if "_id" in book:
            try:
                key = book["_id"]["$oid"]
                return key
            except:
                return str(book["_id"])
        if "$oid" in book:
            return ObjectId(book["$oid"])
        return None

    def getdict(self):
        return {
            "_id": self._id,
            "client": str(self._client),
            "books": [BookModel(db["books"].find_one(ObjectId(x))).getdict() for x in self._books],
            "data": self.showhTime(),
            "valorEmprestimo": self._valor,
            "multa": self.calculateMulta(),
            "valorTotal": self._valor + self._multa
        }

    def getData(self):
        return {
            "client": self._client,
            "books": self._books,
            "data": self._data,
            "valor": float(len(self._books) * 3.0)
        }

    def data(self):
        return {
            "_id": ObjectId(self._id),
            "client": ObjectId(self._client),
            "books": self._books,
            "data": self._data,
            "valor": float(len(self._books) * 3.0)
        }

    def showhTime(self):
        return time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime(self._data))

    def calculateMulta(self):
        multa, juros = self.multaejuros()
        self._multa = float((self._valor * multa + self._valor * juros))
        return self._multa

    def multaejuros(self):
        days = time.localtime(time.time()).tm_yday - time.localtime(self._data).tm_yday
        if days > 12:
            return float((days - 7) * 7.0 / 100.0) + 1, float((days - 7) * 6.0 / 1000.0) + 1
        if days > 10:
            return float(float((days - 7) * 5.0) / float(100.0) + 1), float(float((days - 7) * 4.0) / float(1000.0) + 1)
        if days > 7:
            return float((days - 7) * 3.0 / 100.0 )+ 1, float((days - 7) * 2.0 / 1000.0) + 1
        return 0, 0