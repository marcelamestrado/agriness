# Agriness Developer Backend Challenge
## Desafia para a oportunidade de backend

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Dillinger is a cloud-enabled, mobile-ready, offline-storage compatible,
AngularJS-powered HTML5 Markdown editor.

- Type some Markdown on the left
- See HTML in the right
- ✨Magic ✨

## Instruções

- Instalar o MongoDB https://docs.mongodb.com/manual/installation/
- executar o arquivo app.js que se encontra dentro da pasta /libraryapi
- O aplicativo irá popular automaticamente a base de dados "Books" com os dados dos arquivos .json na raiz do projeto
- Após a inicialização ao app.js o servidor está disponível para uso dos endpoints
- http://127.0.0.1:5000/book que irá listar todos os livros por meio do método GET
- http://127.0.0.1:5000/client/{id}/books por meio do método POST listará todos os livros emprestado pelo cliente

Qualquer dúvida estou a disposição.
